import { Component, AfterViewInit } from '@angular/core';
import { NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
    templateUrl: './dashboard-adm.component.html',
    styleUrls: ['./dashboard-adm.component.scss']
})
export class DashboardAdmComponent implements AfterViewInit {
    subtitle: string;
    constructor() {
        this.subtitle = 'This is some text within a card block.';
    }

    ngAfterViewInit() {
    }
}
