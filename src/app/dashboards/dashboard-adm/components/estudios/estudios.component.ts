import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef
} from '@angular/core';
// import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { TWUtils, NgbDateCustomParserFormatter,
    EstudioService } from '../../../../shared';
import { Router } from '@angular/router';

@Component({
    selector: 'app-estudios',
    // styleUrls: ['./estudios.component.scss'],
    templateUrl: './estudios.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        EstudioService
    ]
})
export class EstudiosComponent implements OnInit {
    public modalTitle = 'Estudios';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataEstudios: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            profesional: { title: 'Profesional', filter: true, },
            paciente: { title: 'Paciente', filter: true, },
            descripcion: { title: 'Descripción', filter: true, },
            fecestudio: {
                title: 'Fecha', filter: false,
                type: 'html',
                width: '20%',
                valuePrepareFunction: (value) => {
                    if (!TWUtils.isNullDate(value)) {
                        return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                    }
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(
        public router: Router,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private estudioService: EstudioService) {
    }

    public ngOnInit(): void {
        this.getListaEstudios();
    }

    getListaEstudios(): any {
        return this.estudioService
            .getListaEstudios()
            .subscribe(estudios => {
                this.dataEstudios = estudios;
                this.source = new LocalDataSource(this.dataEstudios);
            });
    }

    public userRowSelect(event: any): any {
        // this.router.navigate(['/admprofesional', event.data.id]);
    }

    // showMessage(message) {
    //     const msg = `<p class="font-weight-normal">` + message + `</p>`;
    //     const dialogRef = this.modal.alert()
    //         .size('sm')
    //         .showClose(false)
    //         .isBlocking(true)
    //         .title(this.modalTitle)
    //         .body(msg)
    //         .okBtn('Aceptar')
    //         .open();
    // }

    // private handleError(error: any): Promise<any> {
    //     this.showMessage('Error: ' + error);
    //     console.error('An error occurred', error);
    //     return Promise.reject(error.message || error);
    // }
}
