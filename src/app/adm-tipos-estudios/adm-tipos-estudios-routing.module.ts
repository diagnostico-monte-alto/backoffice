import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmTiposEstudiosComponent } from './adm-tipos-estudios.component';

const routes: Routes = [
    {
        path: '',
        component: AdmTiposEstudiosComponent,
        data: {
            title: 'Tipos de estudios',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Tipos de estudios'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmTiposEstudiosRoutingModule { }
