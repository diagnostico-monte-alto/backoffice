import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    TipoEstudio, TipoEstudioService } from '../../../shared';

@Component({
    selector: 'app-tipo-estudio',
    templateUrl: './tipo-estudio.component.html',
    styleUrls: ['./tipo-estudio.component.scss'],
    providers: [
        TipoEstudioService,
        FormBuilder ]
})

export class TipoEstudioComponent implements OnInit, OnChanges {
    @Input() selectedEntity: TipoEstudio;
    @Output() entityUpdated = new EventEmitter();
    openForm = false;

    private selectedID: number;

    formEntity = this.formBuilder.group({
        id: new FormControl({ value: '', disabled: true }),
        tipoestudio: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]),
    });    
    get id() { return this.formEntity.get('id'); }
    get tipoestudio() { return this.formEntity.get('tipoestudio'); }

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private modal: Modal,
        private tipoEstudioService: TipoEstudioService) {
            this.selectedEntity = new TipoEstudio();
    }

    public ngOnInit(): void {
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null && 
            changes['selectedEntity'].currentValue !== undefined) {
            this.formEntity.reset();
            this.openForm = true;
            this.selectedEntity = changes['selectedEntity'].currentValue;
            this.selectedID = this.selectedEntity.id;
            this.id.setValue(this.selectedEntity.id);
            this.tipoestudio.setValue(this.selectedEntity.tipoestudio);
        }
    }

    onSave() {
        this.selectedEntity.tipoestudio = this.tipoestudio.value;

        this.tipoEstudioService
            .save(this.selectedEntity)
            .subscribe(usuario => {
                this.selectedID = usuario.id;
                this.selectedEntity.id = this.selectedID;
                this.openForm = false;
                this.modal.alert()
                    .size('lg')
                    .isBlocking(true)
                    .title('Datos del tipo de estudio')
                    .keyboard(27)
                    .body('Tipo de estudio grabado con éxito.')
                    .okBtn('Aceptar')
                    .open();
                this.onCancelar();
            });
    }

    public onCancelar() {
        this.selectedEntity = null;
        this.openForm = false;
        this.formEntity.reset();
        this.entityUpdated.emit(true);
    }
}
