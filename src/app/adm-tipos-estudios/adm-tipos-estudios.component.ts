import {
    Component, OnInit,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {
    TipoEstudioService, TipoEstudio } from '../shared';

@Component({
    selector: 'app-adm-tipos-estudios',
    styleUrls: ['./adm-tipos-estudios.component.scss'],
    templateUrl: './adm-tipos-estudios.component.html',
    providers: [
        TipoEstudioService
    ]
})
export class AdmTiposEstudiosComponent implements OnInit, AfterViewChecked {
    dataTiposEstudios: Array<any> = Array<any>();
    selectedEntity: TipoEstudio;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Id', filter: true, },
            tipoestudio: { title: 'Tipo de estudio', filter: true, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(
        private cdRef: ChangeDetectorRef,
        private tipoEstudioService: TipoEstudioService) {
    }

    public ngOnInit(): void {
        this.getTiposEstudios();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(status) {
        this.selectedEntity = null;
        this.getTiposEstudios();
    }

    getTiposEstudios(): any {
        return this.tipoEstudioService
            .getList()
            .subscribe(tipos => {
                this.dataTiposEstudios = tipos;
                this.source = new LocalDataSource(this.dataTiposEstudios);
            });
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = this.dataTiposEstudios.find(modulo => modulo.id === event.data.id);
    }
}
