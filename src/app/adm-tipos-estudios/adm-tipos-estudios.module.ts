import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AdmTiposEstudiosComponent } from './adm-tipos-estudios.component';
import { AdmTiposEstudiosRoutingModule } from './adm-tipos-estudios-routing.module';
import { TipoEstudioComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule,
        Ng2SmartTableModule,
        AdmTiposEstudiosRoutingModule
    ],
    declarations: [
        AdmTiposEstudiosComponent,
        TipoEstudioComponent
    ],
    exports: [AdmTiposEstudiosComponent]
})
export class AdmTiposEstudiosModule { }
