import {
    Component, OnInit,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NgbDateCustomParserFormatter, UsuarioService, Usuario } from '../shared';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-adm-usuarios',
    styleUrls: ['./adm-usuarios.component.scss'],
    templateUrl: './adm-usuarios.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        UsuarioService
    ]
})
export class AdmUsuariosComponent implements OnInit, AfterViewChecked {
    selectedEntity: Usuario;
    dataUsuarios: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fotourl: {
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<img src="${value}" class"img_center" alt="Smiley face" height="50">`;
                }
            },
            usuario: { title: 'Usuario', filter: true, },
            apellido: { title: 'Apellido', filter: true, },
            nombre: { title: 'Nombre', filter: true, },
            rol: { title: 'Rol', filter: true, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private cdRef: ChangeDetectorRef,
        private usuarioService: UsuarioService) {
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getUsuarios(undefined);
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(status) {
        this.selectedEntity = null;
        this.getUsuarios(true);
    }

    getUsuarios(tempEntity: any): any {
        return this.usuarioService
            .getList()
            .subscribe(usuarios => {
                this.dataUsuarios = usuarios;
                this.source = new LocalDataSource(this.dataUsuarios);
            });
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Usuario;
    }
}
