import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter,
    Usuario, UsuarioService,
    RolService } from '../../../shared';
import { LocalDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-usuario',
    templateUrl: './usuario.component.html',
    styleUrls: ['./usuario.component.scss'],
    providers: [
        DatePipe,
        UsuarioService,
        RolService,
        FormBuilder ]
})

export class UsuarioComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Usuario;
    @Output() entityUpdated = new EventEmitter();
    openForm = false;

    source: LocalDataSource;
    private selectedID: number;
    private dataRoles: Array<any> = Array<any>();

    private urlPattern = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]' +
        '\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?' +
        '|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.' +
        '(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))' +
        '|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
    formEntity = this.formBuilder.group({
        usuario: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
        apellido: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
        nombre: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]),
        fotourl: new FormControl('',
            [Validators.required, Validators.minLength(4),
            Validators.maxLength(512), Validators.pattern(this.urlPattern)]),
        rol: new FormControl(null, Validators.required),
    });
    get usuario() { return this.formEntity.get('usuario'); }
    get apellido() { return this.formEntity.get('apellido'); }
    get nombre() { return this.formEntity.get('nombre'); }
    get fotourl() { return this.formEntity.get('fotourl'); }
    get rol() { return this.formEntity.get('rol'); }

    public constructor(public router: Router, private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private modal: Modal,
        private usuarioService: UsuarioService,
        private rolService: RolService) {
        this.selectedEntity = new Usuario();
    }

    public ngOnInit(): void {
        this.getListaRoles();
        this.onCancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null &&
            changes['selectedEntity'].currentValue !== undefined) {
            this.formEntity.reset();
            this.openForm = true;
            this.selectedEntity = changes['selectedEntity'].currentValue;
            this.selectedID = this.selectedEntity.id;
            this.usuario.setValue(this.selectedEntity.usuario);
            this.apellido.setValue(this.selectedEntity.apellido);
            this.nombre.setValue(this.selectedEntity.nombre);
            this.fotourl.setValue(this.selectedEntity.fotourl);
            this.rol.setValue(this.selectedEntity.idrol);
        }
    }

    hasEntity(): boolean {
        return this.selectedEntity.id > -1 && this.selectedEntity.id !== undefined;
    }

    getListaRoles(): any {
        return this.rolService
            .getList()
            .subscribe(roles => {
                this.dataRoles = roles;
                this.source = new LocalDataSource(this.dataRoles);
            });
    }

    onResetPassword() {
        const dialogRef = this.modal.confirm()
            .size('lg')
            .isBlocking(true)
            .title('Datos del usuario')
            .keyboard(27)
            .body('¿Desea reestablecer la contraseña a la por defecto?')
            .okBtn('Si')
            .cancelBtn('No')
            .open();
        dialogRef.result
            .then( result => {
                this.resetPassword();
                })
            .catch(() => {});
    }

    resetPassword() {
        this.selectedEntity.contrasena = environment.DEFAULT_PASS;
        this.usuarioService.resetPassword(this.selectedEntity)
            .subscribe(response => {
                this.modal.alert()
                    .size('lg')
                    .isBlocking(true)
                    .title('Datos del usuario')
                    .keyboard(27)
                    .body('Contraseña reestablecida a la por defecto?')
                    .okBtn('Aceptar')
                    .open();
            });
    }

    onNuevo() {
        this.selectedEntity = new Usuario();
        this.openForm = true;
        this.formEntity.reset();
    }

    onSave() {
        this.selectedEntity.usuario = this.usuario.value;
        this.selectedEntity.apellido = this.apellido.value;
        this.selectedEntity.nombre = this.nombre.value;
        this.selectedEntity.fotourl = this.fotourl.value;
        this.selectedEntity.idrol = this.rol.value;

        this.usuarioService
            .save(this.selectedEntity)
            .subscribe(usuario => {
                this.selectedID = usuario.id;
                this.selectedEntity.id = this.selectedID;
                this.openForm = false;
                this.modal.alert()
                    .size('lg')
                    .isBlocking(true)
                    .title('Datos del usuario')
                    .keyboard(27)
                    .body('Usuario grabado con éxito.')
                    .okBtn('Aceptar')
                    .open();
                this.onCancelar();
            });
    }

    onEliminar() {
        this.usuarioService
            .delete(this.selectedEntity.id)
            .subscribe(borrado => {
                if (borrado) {
                    this.modal.alert()
                        .size('lg')
                        .isBlocking(true)
                        .title('Datos del usuario')
                        .keyboard(27)
                        .body('Usuario eliminado con éxito.')
                        .okBtn('Aceptar')
                        .open();
                    this.onCancelar();
                }
            });
    }

    public onCancelar() {
        this.selectedEntity = null;
        this.openForm = false;
        this.formEntity.reset();
        this.entityUpdated.emit(true);
    }
}
