import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AdmUsuariosComponent } from './adm-usuarios.component';
import { AdmUsuariosRoutingModule } from './adm-usuarios-routing.module';
import { UsuarioComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule,
        Ng2SmartTableModule,
        AdmUsuariosRoutingModule
    ],
    declarations: [
        AdmUsuariosComponent,
        UsuarioComponent
    ],
    exports: [AdmUsuariosComponent]
})
export class AdmUsuariosModule { }
