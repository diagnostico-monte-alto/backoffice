import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmUsuariosComponent } from './adm-usuarios.component';

const routes: Routes = [
    {
        path: '',
        component: AdmUsuariosComponent,
        data: {
            title: 'Usuarios',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Usuarios'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmUsuariosRoutingModule { }
