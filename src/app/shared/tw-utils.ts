export class TWUtils {
    static readonly MESSAGE_STAYHERE = 0;
    static readonly MESSAGE_GOTO = 1;
    static readonly MESSAGE_RELOAD = 2;

    static arrayDateToString(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const selDate = arrDate['year'] + '-' + arrDate['month'] + '-' + arrDate['day'];
            return selDate;
        } else { return ''; }
    }

    static arrayDateToStringISO(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const selDate = TWUtils.padLeft(arrDate['year'], '0', 4) +
                TWUtils.padLeft(arrDate['month'], '0', 2) +
                TWUtils.padLeft(arrDate['day'], '0', 2);
            return selDate;
        } else { return ''; }
    }

    static stringDateToJson(strindDate: any): any {
        if (strindDate === '' || strindDate === undefined || strindDate === null) {
            return {};
        }
        const splitDateTime = strindDate.split(' ');
        const datestring = splitDateTime[0].split('-');
        const newDate = new Date(+datestring[0], (+datestring[1]) - 1, +datestring[2], 0, 0, 0, 0);
        return {
             'year': newDate.getFullYear(),
             'month': (newDate.getMonth() + 1),
             'day': newDate.getDate() };
    }

    static isNullDate(arrDate: any) {
        if (arrDate === undefined
            || arrDate === ''
            || arrDate === '0000-00-00'
            || arrDate === '0000-00-00 00:00:00'
            || arrDate === '0'
            || +arrDate === +'0') {
            return true;
        }
        return false;
    }

    static padLeft(text: string, padChar: string, size: number): string {
        return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
    }
}
