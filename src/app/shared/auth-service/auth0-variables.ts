interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

// export const AUTH_CONFIG: AuthConfig = {
//   clientID: 'a9OWiPxe9CurgcQ3pw8diGA49MnuSFv2',
//   domain: 'clickterapia.auth0.com',
//   callbackURL: 'http://localhost:3000/callback'
// };
