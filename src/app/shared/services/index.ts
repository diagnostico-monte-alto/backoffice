export * from './api/base.service';

export * from './api/estudio.service';
export * from './api/moduloxrol.service';
export * from './api/profesional.service';
export * from './api/rol.service';
export * from './api/tipoestudio.service';
export * from './api/usuario.service';
