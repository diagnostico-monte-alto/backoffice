import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog/plugins/bootstrap';


export class Rol {
    public id: number;
    public rol: string;
    constructor() {
        this.id = -1;
    }
}

@Injectable()
export class RolService extends BaseService {
    readonly STATUS_SUCCESS =  'success';

    constructor(httpClient: HttpClient,
        modal: Modal) {
        super(httpClient, modal);
        this.module = 'RolService';
        this.apiUrl += 'rol';
    }

    getList(): Observable<Array<Rol>> {
        const customUrl = `${this.apiUrl}`;

        return this.httpClient.get<Array<Rol>>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response as Array<Rol>;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    getByID(id: number): Observable<Rol> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<any>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response[0] as Rol;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    save(entity: Rol): Observable<Rol> {
        if (entity.id && entity.id > -1) {
            return this.update(entity);
        }
        return this.add(entity);
    }

    add(rol: Rol): Observable<Rol> {
        return this.httpClient.post<Rol>(this.apiUrl, rol, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(rol: Rol): Observable<Rol> {
        return this.httpClient.put<Rol>(this.apiUrl, rol, this.httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    delete(id: number): Observable<boolean> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, this.httpOptions)
            .pipe(
                map(response => {
                    const resp = (response.status[0]);
                    return resp;
                }),
                catchError(this.handleError)
            );
    }
}
