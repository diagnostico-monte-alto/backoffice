import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

export class Estudio {
    public id: string;
    public idpaciente: string;
    public paciente: string;
    public idprofesional: string;
    public profesional: string;
    public descripcion: string;
    public fecestudio: string;
    public fecultmodif: string;

    constructor() {
    }
}

@Injectable()
export class EstudioService extends BaseService {
    readonly GET_LISTAESTUDIOS = 'listaestudios';

    constructor(httpClient: HttpClient,
        protected modal: Modal) {
        super(httpClient, modal);
        this.module = 'EstudioService';
        this.apiUrl += 'estudio';
    }

    getList(): Observable<Array<Estudio>> {
        const customUrl = `${this.apiUrl}`;

        return this.httpClient.get<Array<Estudio>>(customUrl, this.httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError)
            );
    }

    getByID(id: number): Observable<Estudio> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<Estudio>(customUrl, this.httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError)
            );
    }

    getListaEstudios(): Observable<Array<Estudio[]>> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTAESTUDIOS}`;

        return this.httpClient
            .get<Array<Estudio[]>>(customUrl, this.httpOptions)
            .pipe(
                retry(3),
                catchError(this.handleError)
            );
    }
}
