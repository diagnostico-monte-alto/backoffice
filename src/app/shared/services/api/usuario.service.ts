import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

export class Usuario {
    public id: number;
    public usuario: string;
    public apellido: string;
    public nombre: string;
    public contrasena: string;
    public idrol: number;
    public rol: string;
    public fotourl: string;
    public fecultmodif: string;
    constructor() {
        this.id = -1;
        this.contrasena = environment.DEFAULT_PASS;
    }
}

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept': 'application/json',
        'Cache-Control': 'no-cache'
    })
};

@Injectable()
export class UsuarioService extends BaseService {
    readonly STATUS_SUCCESS =  'success';
    readonly PUT_LOGIN = 'login';
    readonly PUT_RESET_PASSWORD = 'reset_password';

    constructor(httpClient: HttpClient,
        modal: Modal) {
        super(httpClient, modal);
        this.module = 'UsuarioService';
        this.apiUrl += 'usuario';
    }

    getList(): Observable<Array<Usuario>> {
        const customUrl = `${this.apiUrl}`;

        return this.httpClient.get<Array<Usuario>>(customUrl, httpOptions)
        .pipe(
            map(response => {
                return response as Array<Usuario>;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    getByID(id: number): Observable<Usuario> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<any>(customUrl, httpOptions)
        .pipe(
            map(response => {
                return response[0] as Usuario;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    login(entity: Usuario): Observable<Usuario> {
        const customUrl = `${this.apiUrl}/${this.PUT_LOGIN}`;
        return this.httpClient.put<any>(customUrl, entity, httpOptions)
        .pipe(
            map(response => {
                return response.message[0] as Usuario;
            }),
            catchError(this.handleError)
        );
    }

    autenticate(usuarioName: string, clave: string): Observable<Boolean> {
        const customUrl = `${this.apiUrl}`;
        const json_req = JSON.stringify({
            'auth':
            {
                'usuario': usuarioName,
                'contrasena': clave
            }
        });

        return this.httpClient.post<Boolean>(customUrl, json_req, httpOptions)
            .pipe(
                map((data: any) => {
                    return data.results.map((entry: any) => (true)
                    );
                }),
                catchError(this.handleError)
            );
    }

    save(usuario: Usuario): Observable<Usuario> {
        if (usuario.id && usuario.id > -1) {
            return this.update(usuario);
        }
        return this.add(usuario);
    }

    add(usuario: Usuario): Observable<Usuario> {
        usuario.contrasena = environment.DEFAULT_PASS;
        return this.httpClient.post<Usuario>(this.apiUrl, usuario, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(usuario: Usuario): Observable<Usuario> {
        return this.httpClient.put<Usuario>(this.apiUrl, usuario, httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    public resetPassword(usuario: Usuario): Observable<boolean> {
        const url = `${this.apiUrl}/${this.PUT_RESET_PASSWORD}`;
        return this.httpClient.put<any>(this.apiUrl, usuario, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    delete(id: number): Observable<boolean> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, httpOptions)
            .pipe(
                map(response => {
                    const resp = (response.status[0]);
                    return resp;
                }),
                catchError(this.handleError)
            );
    }
}
