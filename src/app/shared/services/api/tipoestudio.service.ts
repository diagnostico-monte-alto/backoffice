import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Modal } from 'ngx-modialog/plugins/bootstrap';


export class TipoEstudio {
    public id: number;
    public tipoestudio: string;
    constructor() {
        this.id = -1;
    }
}

@Injectable()
export class TipoEstudioService extends BaseService {
    readonly STATUS_SUCCESS =  'success';

    constructor(httpClient: HttpClient,
        modal: Modal) {
        super(httpClient, modal);
        this.module = 'TipoEstudioService';
        this.apiUrl += 'tipoestudio';
    }

    getList(): Observable<Array<TipoEstudio>> {
        const customUrl = `${this.apiUrl}`;

        return this.httpClient.get<Array<TipoEstudio>>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response as Array<TipoEstudio>;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    getByID(id: number): Observable<TipoEstudio> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.httpClient.get<any>(customUrl, this.httpOptions)
        .pipe(
            map(response => {
                return response[0] as TipoEstudio;
            }),
            retry(3),
            catchError(this.handleError)
        );
    }

    save(entity: TipoEstudio): Observable<TipoEstudio> {
        if (entity.id && entity.id > -1) {
            return this.update(entity);
        }
        return this.add(entity);
    }

    add(entity: TipoEstudio): Observable<TipoEstudio> {
        return this.httpClient.post<TipoEstudio>(this.apiUrl, entity, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    update(entity: TipoEstudio): Observable<TipoEstudio> {
        return this.httpClient.put<TipoEstudio>(this.apiUrl, entity, this.httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    delete(id: number): Observable<boolean> {
        const url = `${this.apiUrl}/${id}`;
        return this.httpClient.delete<any>(url, this.httpOptions)
            .pipe(
                map(response => {
                    const resp = (response.status[0]);
                    return resp;
                }),
                catchError(this.handleError)
            );
    }
}
