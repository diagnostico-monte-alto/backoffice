import { Component, AfterViewInit, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ROUTES } from './menu-items';
import { RouteInfo } from "./sidebar.metadata";
import { Router, ActivatedRoute } from "@angular/router";

import { Profile } from '../guard/auth.guard';
import { Profesional, ProfesionalService, Usuario, UsuarioService } from '../';

declare var $: any;
@Component({
    selector: 'ap-sidebar',
    templateUrl: './sidebar.component.html',
    providers: [
        ProfesionalService,
        UsuarioService]
})
export class SidebarComponent implements OnInit {
    showMenu: string = '';
    showSubMenu: string = '';
    public sidebarnavItems: any[];
    selectedID = '';
    selectedProfile: Profile;
    selectedEntity: Profesional | Usuario;
    selectedUsuario: Usuario;
    nombrePerfil: string;
    locacionPerfil: string;

    //this is for the open close
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';

        } else {
            this.showMenu = element;
        }
    }
    addActiveClass(element: any) {
        if (element === this.showSubMenu) {
            this.showSubMenu = '0';

        } else {
            this.showSubMenu = element;
        }
    }

    constructor(private modalService: NgbModal, private router: Router,
        private route: ActivatedRoute,
        private profesionalService: ProfesionalService,
        private usuarioService: UsuarioService) {
        switch (localStorage.getItem('profile')) {
            case Profile.professional:
                this.selectedEntity = new Profesional();
                break;
            case Profile.admin:
                this.selectedEntity = new Usuario();
                break;
        }
    }
    // End open close
    ngOnInit() {
        this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
        $(function () {
            $(".sidebartoggler").on('click', function () {
                if ($("#main-wrapper").hasClass("mini-sidebar")) {
                    $("body").trigger("resize");
                    $("#main-wrapper").removeClass("mini-sidebar");

                } else {
                    $("body").trigger("resize");
                    $("#main-wrapper").addClass("mini-sidebar");
                }
            });

        });
        this.selectedID = localStorage.getItem('user');
        this.onGetEntityData();
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            switch (localStorage.getItem('profile')) {
                case Profile.professional:
                    this.getProfesional();
                    break;
                case Profile.admin:
                    this.getAdmin();
                    break;
            }
        }
    }

    getProfesional() {
        this.profesionalService
            .getByID(this.selectedID)
            .subscribe(profesional => {
                this.selectedEntity = profesional;
                this.nombrePerfil = this.selectedEntity.nombre + ' ' + this.selectedEntity.apellido;
                this.selectedEntity.fotourl = this.selectedEntity.fotourl;
            });
    }

    getAdmin() {
        const _this = this;
        this.usuarioService
            .getByID(+this.selectedID)
            .subscribe(usuario => {
                _this.selectedEntity = usuario;
                _this.nombrePerfil = _this.selectedEntity.usuario;
                _this.selectedEntity.fotourl = _this.selectedEntity.fotourl;
            });
    }

    public onLogout() {
      switch (localStorage.getItem('profile')) {
        case Profile.admin:
        this.router.navigate(['/authentication/login-adm']);
        break;
        case Profile.professional:
        this.router.navigate(['/authentication/login-pro']);
        break;
        default:
        this.router.navigate(['/authentication/login-adm']);
        break;
      }
      localStorage.clear();
    }

    public hasImage() {
        return (this.selectedEntity.fotourl) ? (this.selectedEntity.fotourl.length > 0) : false;
    }
}
