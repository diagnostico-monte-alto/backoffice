import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmModulosComponent } from './adm-modulos.component';

const routes: Routes = [
    {
        path: '',
        component: AdmModulosComponent,
        data: {
            title: 'Modulos',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Modulos'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmModulosRoutingModule { }
