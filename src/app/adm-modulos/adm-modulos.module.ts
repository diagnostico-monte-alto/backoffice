import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AdmModulosComponent } from './adm-modulos.component';
import { AdmModulosRoutingModule } from './adm-modulos-routing.module';
import { ModuloComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		NgbModule.forRoot(),
		ReactiveFormsModule,
		Ng2SmartTableModule,
		AdmModulosRoutingModule
	],
	declarations: [
		AdmModulosComponent,
		ModuloComponent
	],
	exports: [AdmModulosComponent]
})
export class AdmModulosModule { }
