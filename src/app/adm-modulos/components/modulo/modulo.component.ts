import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder } from '@angular/forms';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    ModuloXRol, ModuloXRolService } from '../../../shared';

@Component({
    selector: 'app-modulo',
    templateUrl: './modulo.component.html',
    styleUrls: ['./modulo.component.scss'],
    providers: [
        ModuloXRolService,
        FormBuilder ]
})

export class ModuloComponent implements OnInit, OnChanges {
    @Input() selectedEntity: ModuloXRol;
    @Output() entityUpdated = new EventEmitter();
    openForm = false;

    private selectedID: number;

    formEntity = this.formBuilder.group({
        rol: new FormControl({ value: '', disabled: true }),
        modulo: new FormControl({ value: '', disabled: true }),
        ver: new FormControl(false),
        editar: new FormControl(false),
        eliminar: new FormControl(false),
    });
    get rol() { return this.formEntity.get('rol'); }
    get modulo() { return this.formEntity.get('modulo'); }
    get ver() { return this.formEntity.get('ver'); }
    get editar() { return this.formEntity.get('editar'); }
    get eliminar() { return this.formEntity.get('eliminar'); }

    public constructor(public router: Router, private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private modal: Modal,
        private moduloXRolService: ModuloXRolService) {
            this.selectedEntity = new ModuloXRol();
    }

    public ngOnInit(): void {
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null &&
            changes['selectedEntity'].currentValue !== undefined) {
            this.formEntity.reset();
            this.openForm = true;
            this.selectedEntity = changes['selectedEntity'].currentValue;
            this.selectedID = this.selectedEntity.id;
            this.rol.setValue(this.selectedEntity.rol);
            this.modulo.setValue(this.selectedEntity.modulo);
            this.ver.setValue(+this.selectedEntity.ver === 1);
            this.editar.setValue(+this.selectedEntity.editar === 1);
            this.eliminar.setValue(+this.selectedEntity.eliminar === 1);
        }
    }

    onSave() {
        this.selectedEntity.rol = this.rol.value;
        this.selectedEntity.modulo = this.modulo.value;
        this.selectedEntity.ver = this.ver.value ? 1 : 0;
        this.selectedEntity.editar = this.editar.value ? 1 : 0;
        this.selectedEntity.eliminar = this.eliminar.value ? 1 : 0;

        this.moduloXRolService
            .save(this.selectedEntity)
            .subscribe(usuario => {
                this.selectedID = usuario.id;
                this.selectedEntity.id = this.selectedID;
                this.openForm = false;
                this.modal.alert()
                    .size('lg')
                    .isBlocking(true)
                    .title('Datos del módulo')
                    .keyboard(27)
                    .body('Permisos del módulo grabados con éxito.')
                    .okBtn('Aceptar')
                    .open();
                this.onCancelar();
            });
    }

    public onCancelar() {
        this.selectedEntity = null;
        this.openForm = false;
        this.formEntity.reset();
        this.entityUpdated.emit(true);
    }
}
