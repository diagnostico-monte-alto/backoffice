import {
    Component, OnInit,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
    RolService, Rol,
    ModuloXRolService, ModuloXRol } from '../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-adm-modulos',
    styleUrls: ['./adm-modulos.component.scss'],
    templateUrl: './adm-modulos.component.html',
    providers: [
        RolService,
        ModuloXRolService
    ]
})
export class AdmModulosComponent implements OnInit, AfterViewChecked {
    selectedRol: Rol;
    dataModulos: Array<any> = Array<any>();
    selectedEntity: ModuloXRol;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            modulo: { title: 'Usuario', filter: true, },
            ver: {
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return (+value === 1) ?
                        `<i class="fa fa-check" aria-hidden="true"></i>` :
                        `<i class="fa fa-times" aria-hidden="true"></i>`;
                }
            },
            editar: {
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return (+value === 1) ?
                        `<i class="fa fa-check" aria-hidden="true"></i>` :
                        `<i class="fa fa-times" aria-hidden="true"></i>`;
                }
            },
            eliminar: {
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return (+value === 1) ?
                        `<i class="fa fa-check" aria-hidden="true"></i>` :
                        `<i class="fa fa-times" aria-hidden="true"></i>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(
        private route: ActivatedRoute,
        private cdRef: ChangeDetectorRef,
        private rolService: RolService,
        private moduloXRolService: ModuloXRolService) {
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getModulosXRol();
        });
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(status) {
        this.selectedEntity = null;
        this.getModulosXRol();
    }

    getModulosXRol(): any {
        return this.moduloXRolService
            .getModulosByIdRol(this.selectedID)
            .subscribe(modulos => {
                this.dataModulos = modulos;
                this.source = new LocalDataSource(this.dataModulos);
            });
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = this.dataModulos.find(modulo => modulo.id === event.data.id);
        // this.selectedEntity = event.data as ModuloXRol;
    }
}
