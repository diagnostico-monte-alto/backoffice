import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmRolesComponent } from './adm-roles.component';

const routes: Routes = [
    {
        path: '',
        component: AdmRolesComponent,
        data: {
            title: 'Roles',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Roles'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmRolesRoutingModule { }
