import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AdmRolesComponent } from './adm-roles.component';
import { AdmRolesRoutingModule } from './adm-roles-routing.module';
import { RolComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule,
        Ng2SmartTableModule,
        AdmRolesRoutingModule
    ],
    declarations: [
        AdmRolesComponent,
        RolComponent
    ],
    exports: [AdmRolesComponent]
})
export class AdmRolesModule { }
