import {
    Component, OnInit,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { RolService, Rol } from '../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-adm-roles',
    styleUrls: ['./adm-roles.component.scss'],
    templateUrl: './adm-roles.component.html',
    providers: [
        RolService
    ]
})
export class AdmRolesComponent implements OnInit, AfterViewChecked {
    selectedEntity: Rol;
    dataRoles: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Rol ID', filter: true, },
            rol: { title: 'Rol', filter: true, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public constructor(
        private cdRef: ChangeDetectorRef,
        private rolService: RolService) {
    }

    public ngOnInit(): void {
        this.getRoles();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(status) {
        this.selectedEntity = null;
        this.getRoles();
    }

    getRoles(): any {
        return this.rolService
            .getList()
            .subscribe(roles => {
                this.dataRoles = roles;
                this.source = new LocalDataSource(this.dataRoles);
            });
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Rol;
    }
}
