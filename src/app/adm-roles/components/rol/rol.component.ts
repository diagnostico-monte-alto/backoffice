import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { Rol, RolService } from '../../../shared';

@Component({
    selector: 'app-rol',
    templateUrl: './rol.component.html',
    styleUrls: ['./rol.component.scss'],
    providers: [
        RolService,
        FormBuilder ]
})

export class RolComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Rol;
    @Output() entityUpdated = new EventEmitter();
    openForm = false;

    private selectedID: number;

    formEntity = this.formBuilder.group({
        id: new FormControl({ value: '', disabled: true }),
        rol: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
    });
    get id() { return this.formEntity.get('id'); }
    get rol() { return this.formEntity.get('rol'); }

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private modal: Modal,
        private rolService: RolService) {
        this.selectedEntity = new Rol();
    }

    public ngOnInit(): void {
        this.onCancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null &&
            changes['selectedEntity'].currentValue !== undefined) {
            this.formEntity.reset();
            this.openForm = true;
            this.selectedEntity = changes['selectedEntity'].currentValue;
            this.selectedID = this.selectedEntity.id;
            this.id.setValue(this.selectedEntity.id);
            this.rol.setValue(this.selectedEntity.rol);
        }
    }

    hasEntity(): boolean {
        return this.selectedEntity.id > -1 && this.selectedEntity.id !== undefined;
    }

    onNuevo() {
        this.selectedEntity = new Rol();
        this.openForm = true;
        this.formEntity.reset();
    }

    onSave() {
        this.selectedEntity.id = this.id.value;
        this.selectedEntity.rol = this.rol.value;

        this.rolService
            .save(this.selectedEntity)
            .subscribe(rol => {
                this.selectedID = rol.id;
                this.selectedEntity.id = this.selectedID;
                this.openForm = false;
                this.modal.alert()
                    .size('lg')
                    .isBlocking(true)
                    .title('Datos del rol')
                    .keyboard(27)
                    .body('Rol grabado con éxito.')
                    .okBtn('Aceptar')
                    .open();
                this.onCancelar();
            });
    }

    onEliminar() {
        const dialogRef = this.modal.confirm()
                .size('lg')
                .isBlocking(true)
                .title('Datos del rol')
                .keyboard(27)
                .body('Elimininará el rol y todos sus privilegios. ¿Desea continuar?')
                .okBtn('Si')
                .cancelBtn('No')
                .open();

        dialogRef.result
            .then( result => {
                this.eliminar();
                })
            .catch(() => {});
    }

    eliminar() {
        this.rolService
            .delete(this.selectedEntity.id)
            .subscribe(borrado => {
                if (borrado) {
                    this.modal.alert()
                        .size('lg')
                        .isBlocking(true)
                        .title('Datos del rol')
                        .keyboard(27)
                        .body('Rol eliminado con éxito.')
                        .okBtn('Aceptar')
                        .open();
                    this.onCancelar();
                }
            });
    }

    onPrivilegios() {
        this.router.navigate(['/adm-modulos', this.id.value]);
    }

    onCancelar() {
        this.selectedEntity = null;
        this.openForm = false;
        this.formEntity.reset();
        this.entityUpdated.emit(true);
    }
}
